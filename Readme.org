** Documentation Labase
Ce dépot contient les sources du site web de /LaBase/.

Le site est consultable à [[https://labase.frama.io/documentation/]].
Il regroupe la documentation des travaux de /LaBase/ et complète les informations disponibles sur le wiki [[https://lebib.org/wiki/doku.php?id=labase:medecine-autonome:covid19]]

** Balisage OrgMode

- le manuel complet d'orgmode : [[https://orgmode.org/orgguide.pdf]]
- Principaux élèments de syntaxe : [[http://karl-voit.at/2017/09/23/orgmode-as-markup-only/]]

** Bibliographie
 [[https://www.zotero.org/groups/2462558/bib]]