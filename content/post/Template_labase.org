
#+title: Template pour Org Mode avec Hugo
#+subtitle: 
#+date: 2020-04-10
#+tags[]: Template Hugo OrgMode
#+draft: true
#+author: 

* Titres 

** Styles
*gras*
/italic/
=literal=
[[https://][lien]]

** Blocs

*** Code : 
Par exemple pour insérer un code html :

#+BEGIN_SRC html

#+END_SRC
*** Tableaux
| 1  | 2 |
| 3  | 4 |

*** CSS
Les blocs peuvent être associés à une class css lors de la convertion vers html :

#+CAPTION: premier tableau
#+ATTR_HTML: :class tableau table-striped table-dark
| 1  | 2 |
| 3  | 4 |